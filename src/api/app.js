const app = require('express')();
const serveStatic = require('serve-static');
const cors = require('cors');

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  app.use(cors());
  next();
});
// Set header to force download
function setJsonHeaders(res, path) {
  res.setHeader('Content-type', 'application/json');
}

// Serve up mock-api folder
app.use(
  '/api',
  serveStatic('mock-api', {
    index: false,
    setHeaders: setJsonHeaders,
  })
);

// Serve up public folder
app.use('/', serveStatic('public', { index: ['index.html', 'index.htm'] }));

app.listen(8888, function () {
  console.log('Acesse: http://localhost:8888');
});
