import React, { useState } from 'react';
import Header from '../../components/header';
import Main from '../../components/main';

import './style.scss';

function ShirtsPage() {
  const [page] = useState('Camisetas');

  return (
    <div id="shoes-page">
      <Header />
      <div className="route">
        <p>Página inicial </p>
        <p>></p>
        <p>{page}</p>
      </div>
      <Main pageName={page} category={4} />
    </div>
  );
}

export default ShirtsPage;
