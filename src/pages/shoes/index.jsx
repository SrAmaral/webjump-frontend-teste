import React, { useState } from 'react';
import Footer from '../../components/footer';

import Header from '../../components/header';
import Main from '../../components/main';

import './style.scss';

function ShoesPage() {
  const [page] = useState('Sapatos');

  return (
    <div id="shoes-page">
      <Header />
      <div className="route">
        <p>Página inicial </p>
        <p>></p>
        <p>{page}</p>
      </div>
      <Main pageName={page} category={3} />
      <Footer />
    </div>
  );
}

export default ShoesPage;
