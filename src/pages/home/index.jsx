import React from 'react';

import Header from '../../components/header';

import './style.scss';

function Home() {
  return (
    <div id="page-app">
      <Header />
      <div className="route">
        <p>Página inicial </p>
        <div className="instructions" />
        <p>
          Para utilizar as funcionalidades, navegue entre estas paginas do menu
          😄
        </p>
      </div>
    </div>
  );
}

export default Home;
