/* eslint-disable jsx-a11y/accessible-emoji */
/* eslint-disable react/button-has-type */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import api from '../../../api/api-connect';
import { Filter } from '../../../services/filter';
import { Order } from '../../../services/order';

import './style.scss';

function RenderItens({
  modeRender,
  category,
  filterColor,
  filterType,
  filterGender,
  order,
  search,
}) {
  const [products, setProducts] = useState([]);
  const [productsTotal, setProductsTotal] = useState();

  useEffect(() => {
    api.get(`api/V1/categories/${category}`).then((response) => {
      const result = response.data;
      let salary;

      if (order === '') {
        salary = result.items;
      } else if (order !== '') {
        salary = Order(order, result.items);
      }

      if (filterColor !== '' || filterType !== '' || filterGender !== '') {
        const filtered = Filter(
          salary,
          filterColor,
          filterGender,
          filterType,
          category
        );

        setProducts(filtered);
      } else {
        setProductsTotal(salary);
        setProducts(salary);
      }
    });
  }, [modeRender, filterColor, filterType, filterGender, order]);

  return (
    <div id="render-list">
      <div className="render-mode-group">
        {products.length === 0 && (
          <div className="not-found-results">
            <p>Nenhum resultado encontrado 😟</p>
          </div>
        )}
        {modeRender &&
          products.map((item) => (
            <div className="card-group" key={item.id}>
              <div className="image-card-group">
                <img src={item.image} alt={`Imagem ${item.name}`} />
              </div>
              <div className="text-group">
                <div className="title-group">
                  <p>{item.name.toUpperCase()}</p>
                </div>
                <div className="price-group">
                  {item.specialPrice && (
                    <>
                      <p
                        style={{
                          color: '#8f9091',
                          fontSize: 12,
                          fontWeight: 600,
                          textDecoration: 'line-through',
                        }}
                      >
                        {item.price.toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })}
                      </p>
                      <p>
                        {item.specialPrice.toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })}
                      </p>
                    </>
                  )}
                  {!item.specialPrice && (
                    <p>
                      {item.price.toLocaleString('pt-br', {
                        style: 'currency',
                        currency: 'BRL',
                      })}
                    </p>
                  )}
                </div>
              </div>
              <button>COMPRAR</button>
            </div>
          ))}
      </div>

      <div className="render-mode-list">
        {!modeRender &&
          products.map((item) => (
            <div className="card-list" key={item.id}>
              <div className="image-card-list">
                <img src={item.image} alt={`Imagem ${item.name}`} />
              </div>
              <div className="text-list">
                <div className="title-list">
                  <p>{item.name.toUpperCase()}</p>
                </div>
              </div>
              <div className="card-button">
                <div className="price-list">
                  {item.specialPrice && (
                    <>
                      <p
                        style={{
                          color: '#8f9091',
                          fontSize: 12,
                          fontWeight: 600,
                          textDecoration: 'line-through',
                        }}
                      >
                        {item.price.toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })}
                      </p>
                      <p>
                        {item.specialPrice.toLocaleString('pt-br', {
                          style: 'currency',
                          currency: 'BRL',
                        })}
                      </p>
                    </>
                  )}
                  {!item.specialPrice && (
                    <p>
                      {item.price.toLocaleString('pt-br', {
                        style: 'currency',
                        currency: 'BRL',
                      })}
                    </p>
                  )}
                </div>
                <button>COMPRAR</button>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
}

export default RenderItens;
