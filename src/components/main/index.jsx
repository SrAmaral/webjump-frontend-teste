/* eslint-disable no-unused-vars */
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React, { useEffect, useState } from 'react';

import GroupImg from '../../assets/icons/group-select.svg';
import GroupUnselectImg from '../../assets/icons/group-unselect.svg';
import ListUnselectImg from '../../assets/icons/list-unselect.svg';
import ListImg from '../../assets/icons/list-select.svg';
import './style.scss';

import RenderItens from './renderItems';
import api from '../../api/api-connect';

// eslint-disable-next-line react/prop-types
function Main({ pageName, category, search }) {
  const [groupMode, setGroupMode] = useState(true);
  const [thisPage, setThisPage] = useState();
  const [products, setProducts] = useState([]);
  const [colorFiltered, setColorFiltered] = useState('');
  const [typeFiltered, setTypeFiltered] = useState('');
  const [genderFiltered, setGenderFiltered] = useState('');
  const [order, setOrder] = useState('');

  function bd() {
    setThisPage(pageName);

    if (thisPage)
      api.get(`api/V1/categories/${category}`).then((response) => {
        const result = response.data;

        setProducts(result.items);
      });
  }

  useEffect(() => {
    bd();
  }, []);

  function initFilters() {
    setColorFiltered('');
    setTypeFiltered('');
    setGenderFiltered('');
  }

  return (
    <div id="main-page">
      <div className="container-market">
        <div className="filter">
          <div className="filter-container">
            <p className="title-filter">FILTRE POR</p>
            {category === 3 && (
              <>
                <p className="title-ul">CORES</p>
                <div className="colors1">
                  <div
                    className="black"
                    onClick={() => setColorFiltered('Preto')}
                    style={
                      colorFiltered === 'Preto'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                  <div
                    className="orange"
                    onClick={() => setColorFiltered('Laranja')}
                    style={
                      colorFiltered === 'Laranja'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                  <div
                    className="yellow"
                    onClick={() => setColorFiltered('Amarela')}
                    style={
                      colorFiltered === 'Amarela'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                </div>
                <div className="colors2">
                  <div
                    className="grey"
                    onClick={() => setColorFiltered('Cinza')}
                    style={
                      colorFiltered === 'Cinza'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                  <div
                    className="blue"
                    onClick={() => setColorFiltered('Azul')}
                    style={
                      colorFiltered === 'Azul'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                  <div
                    className="pink"
                    onClick={() => setColorFiltered('Rosa')}
                    style={
                      colorFiltered === 'Rosa'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                </div>
                <div className="colors3">
                  <div
                    className="beige"
                    onClick={() => setColorFiltered('Bege')}
                    style={
                      colorFiltered === 'Bege'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                </div>

                <p className="title-ul">TIPO</p>
                <ul>
                  <li
                    style={
                      typeFiltered === 'Corrida' ? { color: '#cc0d1f' } : {}
                    }
                    onClick={() => setTypeFiltered('Corrida')}
                  >
                    Corrida
                  </li>
                  <li
                    style={
                      typeFiltered === 'Caminhada' ? { color: '#cc0d1f' } : {}
                    }
                    onClick={() => setTypeFiltered('Caminhada')}
                  >
                    Caminhada
                  </li>
                  <li
                    style={
                      typeFiltered === 'Casual' ? { color: '#cc0d1f' } : {}
                    }
                    onClick={() => setTypeFiltered('Casual')}
                  >
                    Casual
                  </li>
                </ul>
                {(colorFiltered !== '' ||
                  genderFiltered !== '' ||
                  typeFiltered !== '') && (
                  <button className="zerar" onClick={initFilters}>
                    Apagar Filtro
                  </button>
                )}
              </>
            )}
            {category === 2 && (
              <>
                <p className="title-ul">GENERO</p>
                <ul>
                  <li
                    style={
                      genderFiltered === 'Masculina'
                        ? {
                            color: '#cc0d1f',
                          }
                        : {}
                    }
                    onClick={() => setGenderFiltered('Masculina')}
                  >
                    Masculina
                  </li>
                  <li
                    style={
                      genderFiltered === 'Feminina'
                        ? {
                            color: '#cc0d1f',
                          }
                        : {}
                    }
                    onClick={() => setGenderFiltered('Feminina')}
                  >
                    Feminina
                  </li>
                </ul>
                <p className="title-ul">TIPO</p>
                <ul>
                  <li
                    style={
                      typeFiltered === 'Caminhada'
                        ? {
                            color: '#cc0d1f',
                          }
                        : {}
                    }
                    onClick={() => setTypeFiltered('Caminhada')}
                  >
                    Caminhada
                  </li>
                  <li
                    style={
                      typeFiltered === 'Casual'
                        ? {
                            color: '#cc0d1f',
                          }
                        : {}
                    }
                    onClick={() => setTypeFiltered('Casual')}
                  >
                    Casual
                  </li>
                  <li
                    style={
                      typeFiltered === 'Social'
                        ? {
                            color: '#cc0d1f',
                          }
                        : {}
                    }
                    onClick={() => setTypeFiltered('Social')}
                  >
                    Social
                  </li>
                </ul>
                {(colorFiltered !== '' ||
                  genderFiltered !== '' ||
                  typeFiltered !== '') && (
                  <button className="zerar" onClick={initFilters}>
                    Apagar Filtro
                  </button>
                )}
              </>
            )}
            {category === 1 && (
              <>
                <p className="title-ul">CORES</p>
                <div className="colors1">
                  <div
                    className="black"
                    onClick={() => setColorFiltered('Preta')}
                    style={
                      colorFiltered === 'Preta'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                  <div
                    className="orange"
                    onClick={() => setColorFiltered('Laranja')}
                    style={
                      colorFiltered === 'Laranja'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                  <div
                    className="yellow"
                    onClick={() => setColorFiltered('Amarela')}
                    style={
                      colorFiltered === 'Amarela'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                </div>
                <div className="colors2">
                  <div
                    className="pink"
                    onClick={() => setColorFiltered('Rosa')}
                    style={
                      colorFiltered === 'Rosa'
                        ? {
                            outline: 'solid',
                            outlineColor: '#80bdb8',
                          }
                        : {}
                    }
                  />
                </div>

                <p className="title-ul">TIPO</p>
                <ul>
                  <li
                    style={
                      typeFiltered === 'Corrida'
                        ? {
                            color: '#cc0d1f',
                          }
                        : {}
                    }
                    onClick={() => setTypeFiltered('Corrida')}
                  >
                    Corrida
                  </li>
                  <li
                    style={
                      typeFiltered === 'Casual'
                        ? {
                            color: '#cc0d1f',
                          }
                        : {}
                    }
                    onClick={() => setTypeFiltered('Casual')}
                  >
                    Casual
                  </li>
                </ul>
                {(colorFiltered !== '' ||
                  genderFiltered !== '' ||
                  typeFiltered !== '') && (
                  <button className="zerar" onClick={initFilters}>
                    Apagar Filtro
                  </button>
                )}
              </>
            )}
          </div>
        </div>
        <div className="market">
          <p>{thisPage}</p>
          <div className="controller-market">
            <div className="mode-render-album">
              <img
                src={groupMode ? GroupImg : GroupUnselectImg}
                alt="Renderizar por grupo"
                onClick={() => setGroupMode(!groupMode)}
              />
              <img
                src={!groupMode ? ListImg : ListUnselectImg}
                alt="Renderizar por grupo"
                onClick={() => setGroupMode(!groupMode)}
              />
            </div>
            <div className="order">
              <p>ORDENAR POR</p>
              <select onChange={(e) => setOrder(e.target.value)}>
                <option value="">Sem ordenação</option>
                <option value="MAP">Maior Preço</option>
                <option value="MEP">Menor Preço</option>
                <option value="AZ">A - Z</option>
                <option value="ZA">Z - A</option>
              </select>
            </div>
          </div>
          <div className="products">
            <RenderItens
              modeRender={groupMode}
              category={category}
              filterColor={colorFiltered}
              filterType={typeFiltered}
              filterGender={genderFiltered}
              order={order}
              search={search}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Main;
