import React from 'react';
import { Link } from 'react-router-dom';

import './style.scss';

function Menu() {
  return (
    <div className="menu">
      <div className="container">
        <Link to="/home" className="link">
          <p>PÁGINA INICIAL</p>
        </Link>

        <Link to="/shirts" className="link">
          <p>CAMISETAS</p>
        </Link>

        <Link to="/pants" className="link">
          <p>CALÇAS</p>
        </Link>

        <Link to="/shoes" className="link">
          <p>SAPATOS</p>
        </Link>
      </div>
    </div>
  );
}

export default Menu;
