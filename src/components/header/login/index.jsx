/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

import './style.scss';

function Login() {
  return (
    <div id="login">
      <div className="content">
        <a href="">Acesse sua Conta</a>
        <p>ou</p>
        <a href="">Cadastre-se</a>
      </div>
    </div>
  );
}

export default Login;
