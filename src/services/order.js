export function Order(order, array) {
  if (order === '') {
    return array;
  }
  if (order === 'MAP') {
    const items = array;
    let ordenate;
    ordenate = items.sort((a, b) => {
      return b.price - a.price;
    });
    return ordenate;
  }
  if (order === 'MEP') {
    const items = array;
    let ordenate;
    ordenate = items.sort((a, b) => {
      return a.price - b.price;
    });
    return ordenate;
  }
  if (order === 'AZ') {
    const items = array;
    let ordenate;
    ordenate = items.sort((a, b) => {
      return a.name > b.name ? 1 : -1;
    });
    return ordenate;
  }
  if (order === 'ZA') {
    const items = array;
    let ordenate;
    ordenate = items.sort((a, b) => {
      return a.name < b.name ? 1 : -1;
    });
    return ordenate;
  }
}
