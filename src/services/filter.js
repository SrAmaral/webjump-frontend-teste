export function Filter(array, filterColor, filterGender, filterType, category) {
  let result;
  const arrayProducts = array;

  if (category === 1) {
    if (filterColor === '' && filterType !== '') {
      result = arrayProducts.filter((p) => p.type === `${filterType}`);
      return result;
    }
    if (filterColor !== '' && filterType === '') {
      result = arrayProducts.filter(
        (p) => p.filter[0].color === `${filterColor}`
      );
      return result;
    }
    if (filterType !== '' && filterColor !== '') {
      result = arrayProducts
        .filter((p) => p.type === `${filterType}`)
        .filter((p) => p.filter[0].color === `${filterColor}`);

      return result;
    }
  }

  if (category === 2) {
    if (filterGender === '' && filterType !== '') {
      result = arrayProducts.filter((p) => p.type === `${filterType}`);
      return result;
    }
    if (filterGender !== '' && filterType === '') {
      result = arrayProducts.filter(
        (p) => p.filter[0].gender === `${filterGender}`
      );
      return result;
    }
    if (filterType !== '' && filterGender !== '') {
      result = arrayProducts
        .filter((p) => p.type === `${filterType}`)
        .filter((p) => p.filter[0].gender === `${filterGender}`);

      return result;
    }
  }

  if (category === 3) {
    if (filterColor === '' && filterType !== '') {
      result = arrayProducts.filter((p) => p.type === `${filterType}`);
      return result;
    }
    if (filterColor !== '' && filterType === '') {
      result = arrayProducts.filter(
        (p) => p.filter[0].color === `${filterColor}`
      );
      return result;
    }
    if (filterType !== '' && filterColor !== '') {
      result = arrayProducts
        .filter((p) => p.type === `${filterType}`)
        .filter((p) => p.filter[0].color === `${filterColor}`);

      return result;
    }
  }
}
