# **Teste WebJump Front-end**

## **Iniciando o Projeto**

Para iniciar o uso do projeto é necessário rodar alguns comandos.
- Primeiramente é necessário clonar o repositório para sua maquina. Para isto é necessário ter o git instalado em sua maquiná. [Instalação do git](https://git-scm.com/book/pt-br/v2/Começando-Instalando-o-Git)
	
```bash
  git clone https://SrAmaral@bitbucket.org/SrAmaral/webjump-frontend.git
```
- Feito o clone do repositório, é preciso alterar a brench para a que contem o código desenvolvido. Dentro da pasta clonada digite o comando em seu terminal
```bash
  git checkout Desafio
```
- Precisamos agora instalar as dependências do nosso projeto, para isto passamos o código
 ```bash
  yarn install
 ```

>❗❗❗ Atenção, neste passo a cima poderá haver algum erro, pois caso não tenha o yarn instalado em sua maquina, devera ser instalado, ou caso deseje utilizar o npm. Ainda sim em ambas as opções é necessário tem o node instalado em sua maquina. [Instalação do node](https://nodejs.org/en/)

- Feito estes passos acima, podemos dar continuidade, e iniciar o projeto. O projeto foi dividido em duas partes, por este motivo precisaremos de duas janelas de terminal. Em uma janela inicie o app com o comando.

- Em outra janela inicie o back-end com o comando.
```bash
  yarn start-back
  ou
  npm run start-back
```

```bash
  yarn start-front
  ou
  npm run start-front
```
> ❗Valido ressaltar que se não inciar o back-end, os produtos, não iram aparecer.

Feito essas etapas o projeto estará rodando em seu navegador e pronto para ser utilizado e avaliado 😁....
> Caso queira rodar o o aplicativo em um celular ou outro dispositivo, o app também irá funcionar, no entanto é necessário algumas alterações para tal.
> O app funciona no http://localhost:3000 . No entando caso queira rodar o app no celular por exemplo, é necessário utilizar o ip da rede em comum entre os dispositovos. Exemplo:
> *No computador a rede selecionada é  o ***wifi x***  que por sua vez possui o ip, ***192.151.0.1*** neste caso, ao abrir o navegador de seu dispositivo é necessario acessar esta rota. http://192.151.0.1:3000,  ❗❗ Atenção, ao acessar o app por outro dispositivo, a rota do back end não ira funcionar, sendo assim deve ser alterada no código seguindo os passos a seguir.
> Dentro da pasta do projeto, tera uma pasta ***src***, dentro desta pasta terá a pasta ***api***, altere o ***baseURL*** dentro do arquivo ***api-connect.js*** para o ip de sua rede exemplo: http://192.151.0.1:8888 , acrescentando o ***8888***  como porta.

Agora sim, chegamos ao final 😄

## **Tecnologias**
- ReactJs -> para a construção da aplicação.
- Express -> para o back end, visto que o com o connect estava tendo trava nas requisições do app, com isto também, foi utilizada a biblioteca cors para conceder o acesso as requisições e as origens que poderão acessar a api.
- React-Router -> para criação das rotas de navegação do app.
- Axios -> para conectar a api com o app e fazer suas requisições.

## **Soluções Adotadas**
- O site possui 4 paginas.
- Nestas paginas, o header e o conteudo são componentes.
- O menu de filtros e o container dos produtos, são apenas 1 componente que recebe a pagina atual por propriedades e faz a requisição a api, a partir desta propriedade. Assim como o filtro é alterado a partir da propriedade recebida da pagina atual. 
- As funcionalidades de filtros, ordenação, e modo de exibição dos produtos, foram implementadas.
- Cada tela de produtos, a partir da api possuem duas possibilidades de filtros, que podem ser combinadas entre si. Podendo assim filtrar por estas 2 características.
- A ordenação de items se da por ordem alfabética, ou por valor do produto.
- A escolha de modo de exibição segue as da referencia enviada. sendo por blocos, ou bloco unico inline.
- Os breackPoints nas resoluções de 320px, 768px, 1024px e 1440px, foram implementados, tendo apenas adição no header e produtos com um breackPoint em 462, para melhor flexibilidade da pagina.
- Os filtros dos produtos são implementados logo apos o retorno da requisição na api.



### Agradecimentos
Agradeço verdadeiramente a oportunidade de participar do teste de seleção da WebJump.
Fico realmente lisonjeado em ter a oportunidade de concorrer a possibilidade de  participar desta equipe. E finalizo dizendo que todos precisamos de apenas uma oportunidade para mostrarmos nosso potencial e evoluir com isto.

**Muito obrigado !!!** 😃😃